/*
 * task.c
 *
 *  Created on: 2020/03/16
 *      Author: �@��Y
 */

#include <stdlib.h>
#include "task.h"



void init_task(TASK_LIST_t*tasks){
	tasks->head=NULL;
	tasks->tail=NULL;
	tasks->count=0;
}
int create_task(TASK_LIST_t*tasks,void(*process)(void)){
	TCB_t*new_task=(TCB_t*)malloc(sizeof(TCB_t));
	if(new_task==NULL){//severe eror
		return -1;
	}
	new_task->process=process;
	new_task->task_id=tasks->count;
	new_task->next=NULL;
	if(tasks->head==NULL){
		tasks->head=new_task;
		tasks->tail=new_task;
	}else{
		new_task->prev=tasks->tail;
		tasks->tail->next=new_task;
		tasks->tail=new_task;

	}
	tasks->count++;
	return new_task->task_id;

}

void set_task_status(TASK_LIST_t*tasks,int task_id,int new_state){
	TCB_t*now=tasks->head;
	if(now==NULL){
		return;
	}
	while(now->next!=NULL){
		if(now->task_id==task_id){
			now->status=new_state;
		}
	}

}
void activate_task(TASK_LIST_t*tasks,int task_id){
	set_task_status(tasks,task_id,TASK_ACTIVE);
}
void deactivate_task(TASK_LIST_t*tasks,int task_id){
	set_task_status(tasks,task_id,TASK_INACTIVE);
}
void do_task(TASK_LIST_t*tasks){
	TCB_t*now=tasks->head;
	if(now==NULL){
		return;
	}
	while(now->next!=NULL){
		if(now->status==TASK_ACTIVE){
			now->process();
		}
	}

}
