#ifndef __TIMER_H__
#define __TIMER_H__ 1
#ifndef TIMSK1 
#define TIMSK1 TIMSK
#endif
void timer_init(unsigned int period,void(*p_func)(void));
void timer_init_us(unsigned int period,void(*p_func)(void));//us
#endif
