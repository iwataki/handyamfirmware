/*
 * task.h
 *
 *  Created on: 2020/03/16
 *      Author: �@��Y
 */

#ifndef TASK_H_
#define TASK_H_
#include<inttypes.h>
#define TASK_ACTIVE 1
#define TASK_INACTIVE 0

typedef struct TCB{
	struct TCB*next;
	struct TCB*prev;
	uint8_t status;
	int task_id;
	void(*process)(void);
}TCB_t;

typedef struct{
	TCB_t*head;
	TCB_t*tail;
	int count;

}TASK_LIST_t;

void init_task(TASK_LIST_t*tasks);
int create_task(TASK_LIST_t*tasks,void(*process)(void));
void activate_task(TASK_LIST_t*tasks,int task_id);
void deactivate_task(TASK_LIST_t*tasks,int task_id);
void do_task(TASK_LIST_t*tasks);


#endif /* TASK_H_ */
