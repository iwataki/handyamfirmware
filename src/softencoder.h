#ifndef __SOFTENC_H__
//IOポートから低速のロータリエンコーダを読む
#define __SOFTENC_H__

#include <inttypes.h>
#include <avr/io.h>
//A相B相のつながっているピンを指定
/*#define DDR_PHASE_A DDRD
#define DDR_PHASE_B DDRD

#define PIN_PHASE_A	PIND
#define PIN_PHASE_B PIND

#define PORT_PHASE_A PORTD
#define PORT_PHASE_B PORTD

#define PHASE_A 0
#define PHASE_B 1*/

typedef struct{
	uint16_t prev_count;
	uint16_t latest_count;
	uint8_t*DDR_phaseA;
	uint8_t*DDR_phaseB;
	uint8_t*PIN_phaseA;
	uint8_t*PIN_phaseB;
	uint8_t*PORT_phaseA;
	uint8_t*PORT_phaseB;
	uint8_t bitA;
	uint8_t bitB;
	uint8_t direction;
	uint8_t prev_portval;
	int8_t prev_direction;
}ENCODER_t;


void init_enc(ENCODER_t*enc,int dir);
void set_encoder_val(ENCODER_t*enc,int val);
int get_encoder_val(ENCODER_t*enc);
int get_encoder_diff(ENCODER_t*enc);
void update_encoder(ENCODER_t*enc);
#endif
