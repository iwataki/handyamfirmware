/*
 * config_memory.h
 *
 *  Created on: 2019/08/21
 *      Author: �@��Y
 */

#ifndef CONFIG_MEMORY_H_
#define CONFIG_MEMORY_H_
#include <stdint.h>
#include "data_structure.h"
typedef struct{
	CALIBRATION_DATA_t calibration;
	VFO_CONFIG_t vfo_a;
	VFO_CONFIG_t vfo_b;
}SystemNonvolatileConfig_t;

void save_config(SystemNonvolatileConfig_t*cfg);
//success: return 1;
//fail: return 0;
int load_config(SystemNonvolatileConfig_t*cfg);


#endif /* CONFIG_MEMORY_H_ */
