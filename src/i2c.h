/*! \file i2ceeprom.h \brief Interface for standard I2C EEPROM memories. */
//*****************************************************************************
//
// File Name	: 'i2ceeprom.h'
// Title		: Interface for standard I2C EEPROM memories
// Author		: Pascal Stang - Copyright (C) 2003
// Created		: 2003.04.23
// Revised		: 2003.04.23
// Version		: 0.1
// Target MCU	: Atmel AVR series
// Editor Tabs	: 4
//
///	\ingroup driver_hw
/// \defgroup i2ceeprom Interface for standard I2C EEPROM memories (i2ceeprom.c)
/// \code #include "i2ceeprom.h" \endcode
/// \par Overview
///		This library provides functions for reading and writing standard
///	24Cxxx/24LCxxx I2C EEPROM memories.  Memory sizes up to 64Kbytes are
///	supported.  Future revisions may include page-write support.
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//
//*****************************************************************************

#ifndef __I2CEEPROM_H__
#define __I2CEEPROM_H__

#define SEND_START_COND 0xa4
#define SEND_STOP_COND 0x94
#define I2C_TRANSACTION_START 0x84

// functions

//! Initialize I2C interface
void i2c_init(void);
//! send I2C start condition
char i2c_start(void);
//! send I2C stop condition
void i2c_stop(void);
//! send I2C device addres and r/w flag
char i2c_send_addr(unsigned char devaddr);
//! send I2C data
char i2c_send(int size,unsigned char*data);
//! receive I2C data
char i2c_receive(int size,unsigned char*data);
//! write I2C
void i2c_write(int addr,unsigned char*data,int len);
#endif
