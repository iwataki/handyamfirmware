/*
 * data_structure.h
 *
 *  Created on: 2020/03/16
 *      Author: 宗一郎
 */

#ifndef DATA_STRUCTURE_H_
#define DATA_STRUCTURE_H_
#include <inttypes.h>

//RITの状態を表す構造体
typedef struct {
	uint8_t is_enable;
	uint16_t frequency;
}RIT_CONFIG_t;

//VFOの状態を表す構造体
typedef struct{
	uint16_t step;
	uint32_t frequency;
	RIT_CONFIG_t rit_config;
}VFO_CONFIG_t;

//IF周波数，クロック源の周波数キャリブレーションデータを表す構造体
typedef struct{
	uint32_t IF;
	uint32_t crystal;
}CALIBRATION_DATA_t;

//電源の状態を表す構造体
typedef struct{
	uint16_t voltage;
	uint8_t is_ac_powered;
	uint8_t is_charging;
}POWER_STATE_t;

//通常動作時のステートマシン状態
typedef struct{
	uint8_t is_transmit;

}NORMAL_MODE_STATE_t;



//動作中のシステムの状態
typedef struct{
	VFO_CONFIG_t vfo_a;
	VFO_CONFIG_t vfo_b;
	uint8_t active_vfo;
	CALIBRATION_DATA_t calibration;
	POWER_STATE_t power_status;
	uint16_t rssi;

}SYSTEM_STATUS_t;

#endif /* DATA_STRUCTURE_H_ */
