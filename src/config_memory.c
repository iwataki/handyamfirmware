/*
 * config_memory.c
 *
 *  Created on: 2019/08/21
 *      Author: �@��Y
 */
#include <avr/eeprom.h>
#include <util/crc16.h>
#include <string.h>
#include "config_memory.h"

static uint16_t config_crc __attribute__((section(".eeprom")));
static SystemNonvolatileConfig_t system_config_memory __attribute__((section(".eeprom")));

void save_config(SystemNonvolatileConfig_t*cfg){
	uint8_t buffer[sizeof(SystemNonvolatileConfig_t)];
	memcpy(buffer,cfg,sizeof(SystemNonvolatileConfig_t));
	uint16_t crc=0xffff;//crc 16 initial value 0xffff
	for(int  i=0;i<sizeof(SystemNonvolatileConfig_t);i++){
		crc=_crc16_update(crc,buffer[i]);
	}
	eeprom_write_block(buffer,&system_config_memory,sizeof(SystemNonvolatileConfig_t));
	eeprom_busy_wait();
	eeprom_write_block(&crc,&config_crc,sizeof(uint16_t));
	eeprom_busy_wait();
}
//success: return 1;
//fail: return 0;
int load_config(SystemNonvolatileConfig_t*cfg){
	uint8_t buffer[sizeof(SystemNonvolatileConfig_t)];
	eeprom_read_block(buffer,&system_config_memory,sizeof(SystemNonvolatileConfig_t));
	uint16_t crc_mem;
	eeprom_read_block(&crc_mem,&config_crc,sizeof(uint16_t));
	uint16_t crc=0xffff;
	for(int  i=0;i<sizeof(SystemNonvolatileConfig_t);i++){
		crc=_crc16_update(crc,buffer[i]);
	}
	if(crc!=crc_mem){
		return 0;//crc mismatch. fail
	}
	memcpy(cfg,buffer,sizeof(SystemNonvolatileConfig_t));
	return 1;
}
